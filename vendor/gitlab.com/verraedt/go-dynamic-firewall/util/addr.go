package util

import (
	"context"
	"net"

	"github.com/vishvananda/netlink"

	log "github.com/sirupsen/logrus"
)

// GetAddressRecords returns a list of proposed A and AAAA dns records, indexed by the record type.
func GetAddressRecords(ips []net.IP) (map[string][]net.IP, error) {
	a := []net.IP{}
	aaaaMacBased := []net.IP{}
	aaaaOther := []net.IP{}

	for _, addr := range ips {
		switch {
		case addr.To4() != nil:
			a = append(a, addr)
		case IsMacBased(addr):
			aaaaMacBased = append(aaaaMacBased, addr)
		default:
			aaaaOther = append(aaaaOther, addr)
		}
	}

	result := map[string][]net.IP{}

	if len(a) > 0 {
		result["A"] = a
	}

	if len(aaaaMacBased) > 0 {
		result["AAAA"] = aaaaMacBased
	} else if len(aaaaOther) > 0 {
		result["AAAA"] = aaaaOther
	}

	return result, nil
}

// GetPublicAddresses returns a list of public IP addresses on the host.
func GetPublicAddresses() ([]net.IP, error) {
	ifaces, err := net.Interfaces()
	if err != nil {
		return nil, err
	}

	return GetPublicAddressesFromIfaces(ifaces)
}

// GetPublicAddressesFromIfaces returns a list of public IP addresses on the given interfaces.
func GetPublicAddressesFromIfaces(ifaces []net.Interface) ([]net.IP, error) {
	result := []net.IP{}

	for _, iface := range ifaces {
		addrs, err := iface.Addrs()
		if err != nil {
			return nil, err
		}

		for _, addr := range addrs {
			var ip net.IP

			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}

			if ip.IsGlobalUnicast() && !IsLocal(ip) {
				result = append(result, ip)
			}
		}
	}

	return result, nil
}

// IsLocal returns true on local ip addresses.
func IsLocal(ip net.IP) bool {
	if ip4 := ip.To4(); ip4 != nil {
		return ip4[0] == 10 ||
			(ip4[0] == 172 && ip4[1]&0xf0 == 16) ||
			(ip4[0] == 192 && ip4[1] == 168)
	}

	return len(ip) == net.IPv6len && ip[0]&0xfe == 0xfc
}

// IsMacBased returns true on mac based ip addresses.
func IsMacBased(ip net.IP) bool {
	if ip.To4() != nil || len(ip) != net.IPv6len {
		return false
	}

	return ip[11] == 0xff && ip[12] == 0xfe
}

// An AddressListener represents a listening object on address changes.
type AddressListener struct {
	Ifaces    []net.Interface
	Channel   <-chan []net.IP
	netlinkCh <-chan netlink.AddrUpdate
	lastIPs   []net.IP
}

// NewAddressListener creates a new AddressListener.
func NewAddressListener(ctx context.Context) (*AddressListener, error) {
	ifaces, err := net.Interfaces()
	if err != nil {
		return nil, err
	}

	return NewAddressListenerIfaces(ctx, ifaces)
}

// NewAddressListenerIfaces creates a new AddressListener for given interfaces.
func NewAddressListenerIfaces(ctx context.Context, ifaces []net.Interface) (*AddressListener, error) {
	var (
		ch        = make(chan []net.IP, 1)
		netlinkCh = make(chan netlink.AddrUpdate)
		err       error
	)

	a := &AddressListener{
		Ifaces:    ifaces,
		Channel:   ch,
		netlinkCh: netlinkCh,
	}

	a.lastIPs, err = GetPublicAddressesFromIfaces(ifaces)
	if err != nil {
		return nil, err
	}

	err = netlink.AddrSubscribe(netlinkCh, ctx.Done())
	if err == nil {
		ch <- a.lastIPs

		go a.listen(ctx, ch)
	}

	return a, err
}

func (a *AddressListener) listen(ctx context.Context, ch chan<- []net.IP) {
	defer close(ch)

	for {
		select {
		case <-ctx.Done():
			return

		case addrevent, ok := <-a.netlinkCh:
			if !ok {
				return
			}

			if !a.isMyIface(addrevent.LinkIndex) {
				continue
			}

			ip := addrevent.LinkAddress.IP
			if ip.IsGlobalUnicast() && !IsLocal(ip) && a.isAddrChange(ip, addrevent.NewAddr) {
				ips, err := GetPublicAddressesFromIfaces(a.Ifaces)
				if err != nil {
					log.Warnf("ignoring err: %s", err)

					continue
				}

				a.lastIPs = ips
				ch <- ips
			}
		}
	}
}

func (a *AddressListener) isMyIface(index int) bool {
	for _, iface := range a.Ifaces {
		if iface.Index == index {
			return true
		}
	}

	return false
}

func (a *AddressListener) isAddrChange(ip net.IP, newAddr bool) bool {
	if a.lastIPs == nil {
		return true
	}

	found := false
	ipv6MacBased := false

	for _, currentIP := range a.lastIPs {
		if ip.Equal(currentIP) {
			found = true
		}

		if currentIP.To4() == nil && IsMacBased(currentIP) {
			ipv6MacBased = true
		}
	}

	switch {
	case found:
		return !newAddr
	case !found && !newAddr:
		return false
	case ip.To4() != nil:
		return true
	case ipv6MacBased:
		return IsMacBased(ip)
	default:
		return true
	}
}
