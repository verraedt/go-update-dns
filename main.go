package main

import (
	"context"
	"log"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

func main() {
	hostname, err := os.Hostname()
	if err != nil {
		log.Fatal(err)
	}

	var (
		updater Updater
		debug   bool
	)

	rootCmd := &cobra.Command{
		Use:   "update-dns",
		Short: "Update digitalocean dns records for this host",
		Args:  cobra.NoArgs,
		RunE: func(cmd *cobra.Command, args []string) error {
			if debug {
				logrus.SetLevel(logrus.DebugLevel)
			}

			updater.Context = context.Background()

			return updater.Run()
		},
	}

	rootCmd.Flags().BoolVarP(&updater.Loop, "wait", "d", false, "Wait for changes in ip addresses and apply them immediately (daemon mode)")
	rootCmd.Flags().StringVar(&updater.Hostname, "hostname", hostname, "Hostname to register IPv6 addresses, defaults to /etc/hostname")
	rootCmd.Flags().StringVar(&updater.HostnameIPv4, "hostname-ipv4", "", "Hostname to register IPv4 addresses, defaults to --hostname")
	rootCmd.Flags().StringVar(&updater.PublicInterface, "interface", "", "Public interface to select ip addresses from, defaults to autodetect")
	rootCmd.Flags().StringSliceVar(&updater.PublicAddresses, "address", nil, "Staticly assigned public ip addresses, can be used to override autodetected public addresses")
	rootCmd.Flags().StringVar(&updater.DoToken, "do-token", os.Getenv("DIGITALOCEAN_TOKEN"), "Digitalocean API token")
	rootCmd.Flags().StringVar(&updater.DoDomain, "do-domain", "ext.verraedt.be", "Digitalocean domain")
	rootCmd.Flags().BoolVarP(&debug, "debug", "v", false, "Verbose output for debugging")

	if err := rootCmd.Execute(); err != nil {
		logrus.Fatal(err)
	}
}
