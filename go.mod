module gitlab.com/verraedt/go-dns-update

go 1.12

require (
	github.com/42wim/ipsetd v0.0.0-20190525215909-00ed106ef073 // indirect
	github.com/alecthomas/template v0.0.0-20160405071501-a0175ee3bccc // indirect
	github.com/alecthomas/units v0.0.0-20151022065526-2efee857e7cf // indirect
	github.com/chzyer/logex v1.1.10 // indirect
	github.com/chzyer/readline v0.0.0-20180603132655-2972be24d48e // indirect
	github.com/chzyer/test v0.0.0-20180213035817-a1ea475d72b1 // indirect
	github.com/digitalocean/godo v1.15.0 // indirect
	github.com/elgs/gojq v0.0.0-20160421194050-81fa9a608a13 // indirect
	github.com/elgs/gosplitargs v0.0.0-20161028071935-a491c5eeb3c8 // indirect
	github.com/elgs/jsonql v0.0.0-20181106040245-00e077ad9cda // indirect
	github.com/fsouza/go-dockerclient v1.4.1 // indirect
	github.com/hashicorp/consul/api v1.1.0 // indirect
	github.com/shibukawa/configdir v0.0.0-20170330084843-e180dbdc8da0 // indirect
	github.com/sirupsen/logrus v1.3.0
	github.com/spf13/cobra v1.0.0
	github.com/tehnerd/gnl2go v0.0.0-20161218223753-101b5c6e2d44 // indirect
	github.com/vishvananda/netlink v1.0.0 // indirect
	github.com/vishvananda/netns v0.0.0-20180720170159-13995c7128cc // indirect
	gitlab.com/verraedt/go-dynamic-firewall v0.0.0-20220215085821-52a83e6c7f68
	golang.org/x/crypto v0.0.0-20190605123033-f99c8df09eb5 // indirect
	golang.org/x/oauth2 v0.0.0-20190523182746-aaccbc9213b0 // indirect
	golang.org/x/sys v0.0.0-20190528012530-adf421d2caf4 // indirect
	gopkg.in/alecthomas/kingpin.v2 v2.2.6 // indirect
)
