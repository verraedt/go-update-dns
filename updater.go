package main

import (
	"context"
	"net"

	"gitlab.com/verraedt/go-dynamic-firewall/util"
)

type Updater struct {
	Hostname        string
	HostnameIPv4    string
	PublicInterface string
	PublicAddresses []string
	DoDomain        string
	DoToken         string
	Loop            bool
	context.Context
}

func (u *Updater) Run() error {
	listener, err := u.AddressListener()
	if err != nil {
		return err
	}

	for {
		ips := <-listener.Channel

		ips = u.OverridePublicAddresses(ips)

		err = u.Update(ips)
		if err != nil {
			return err
		}

		if !u.Loop {
			return nil
		}
	}
}

func (u *Updater) OverridePublicAddresses(ips []net.IP) []net.IP {
	if len(u.PublicAddresses) == 0 {
		return ips
	}

	result := []net.IP{}

	var has4, has6 bool

	for _, addr := range u.PublicAddresses {
		ip := net.ParseIP(addr)

		if ip == nil {
			continue
		}

		result = append(result, ip)

		if ip.To4() != nil {
			has4 = true
		} else {
			has6 = true
		}
	}

	for _, ip := range ips {
		if ip.To4() != nil && has4 {
			continue
		}

		if ip.To4() == nil && has6 {
			continue
		}

		result = append(result, ip)
	}

	return result
}

func (u *Updater) AddressListener() (*util.AddressListener, error) {
	if u.PublicInterface == "" {
		return util.NewAddressListener(u.Context)
	}

	iface, err := net.InterfaceByName(u.PublicInterface)
	if err != nil {
		return nil, err
	}

	return util.NewAddressListenerIfaces(u.Context, []net.Interface{*iface})
}

func (u *Updater) Update(ips []net.IP) error {
	records, err := util.GetAddressRecords(ips)
	if err != nil {
		return err
	}

	if a, ok := records["A"]; ok && u.HostnameIPv4 != "" && u.HostnameIPv4 != u.Hostname {
		copy := map[string][]net.IP{
			"A": a,
		}

		err = util.UpdateDigitalOcean(u.DoToken, u.DoDomain, u.HostnameIPv4, copy)
		if err != nil {
			return err
		}

		delete(records, "A")
	}

	return util.UpdateDigitalOcean(u.DoToken, u.DoDomain, u.Hostname, records)
}
